#[derive(Debug, Clone)]
pub struct HttpHeader {
    name: String,
    value: String,
}

impl HttpHeader {
    pub fn new(name: String, value: String) -> HttpHeader {
        HttpHeader { name, value }
    }

    pub fn name(&self) -> &String {
        &self.name
    }

    pub fn value(&self) -> &String {
        &self.value
    }

    pub fn as_bytes(&self) -> Vec<u8> {
        format!("{}: {}\r\n", self.name, self.value).as_bytes().to_vec()
    }
}
