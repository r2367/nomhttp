#[derive(Debug)]
pub struct HttpChunk {
    raw: Vec<u8>,
    body: Vec<u8>,
    size: usize,
}

impl HttpChunk {
    pub fn new(raw: Vec<u8>, body: Vec<u8>, size: usize) -> Self {
        HttpChunk { raw, body, size }
    }

    pub fn raw(&self) -> &Vec<u8> {
        &self.raw
    }

    pub fn body(&self) -> &Vec<u8> {
        &self.body
    }

    pub fn size(&self) -> usize {
        self.size
    }
}
