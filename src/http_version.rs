#[derive(Debug, Clone)]
pub enum HttpVersion {
    Http09,
    Http10,
    Http11,
    Http2,
    Http3,
}

impl Default for HttpVersion {
    fn default() -> Self {
        HttpVersion::Http11
    }
}
#[derive(Debug)]
pub struct HttpVersionParsingError;

impl std::fmt::Display for HttpVersionParsingError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "Error parsing http version from HTTP message")
    }
}

impl std::str::FromStr for HttpVersion {
    type Err = HttpVersionParsingError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "HTTP/0.9" => Ok(HttpVersion::Http09),
            "HTTP/1.0" => Ok(HttpVersion::Http10),
            "HTTP/1.1" => Ok(HttpVersion::Http11),
            "HTTP/2.0" => Ok(HttpVersion::Http2),
            "HTTP/3.0" => Ok(HttpVersion::Http3),
            _ => Err(HttpVersionParsingError),
        }
    }
}

impl std::fmt::Display for HttpVersion {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        let s = match self {
            HttpVersion::Http09 => "HTTP/0.9",
            HttpVersion::Http10 => "HTTP/1.0",
            HttpVersion::Http11 => "HTTP/1.1",
            HttpVersion::Http2 => "HTTP/2",
            HttpVersion::Http3 => "HTTP/3",
        };
        write!(f, "{}", s)
    }
}
