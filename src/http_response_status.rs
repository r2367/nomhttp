use crate::http_version::HttpVersion;

#[derive(Debug, Clone)]
pub struct HttpResponseStatus {
    version: HttpVersion,
    code: usize,
    msg: Vec<u8>,
}

impl Default for HttpResponseStatus {
    fn default () -> Self {
        HttpResponseStatus {
            version: HttpVersion::default(),
            code: 200,
            msg: b"OK".to_vec(),
        }
    }
}
impl std::fmt::Display for HttpResponseStatus {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{}/{}", self.code(), String::from_utf8_lossy(self.msg()))
    }
}
impl HttpResponseStatus {
    pub fn new(version: HttpVersion, code: usize, msg: Vec<u8>) -> Self {
        HttpResponseStatus { version, code, msg }
    }

    pub fn code(&self) -> usize {
        self.code
    }

    pub fn msg(&self) -> &Vec<u8> {
        &self.msg
    }

    pub fn version(&self) -> &HttpVersion {
        &self.version
    }
}
