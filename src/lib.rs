pub mod http_chunk;
pub mod http_header;
pub mod http_method;
pub mod http_request;
pub mod http_response;
pub mod http_response_status;
pub mod http_transfer_encoding;
pub mod http_version;
pub mod parsers;


#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        let result = 2 + 2;
        assert_eq!(result, 4);
    }
}
