#[derive(Debug, PartialEq, Clone)]
pub enum HttpMethod {
    Get,
    Post,
    Trace,
    Head,
    Put,
    Delete,
    Connect,
    Options,
    Patch,
    Other(String),
}

impl std::fmt::Display for HttpMethod {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        let s = match self {
            HttpMethod::Get => "GET",
            HttpMethod::Put => "PUT",
            HttpMethod::Post => "POST",
            HttpMethod::Trace => "TRACE",
            HttpMethod::Head => "HEAD",
            HttpMethod::Delete => "DELETE",
            HttpMethod::Options => "OPTIONS",
            HttpMethod::Connect => "CONNECT",
            HttpMethod::Patch => "PATCH",
            HttpMethod::Other(raw) => raw,
        };

        write!(f, "{}", s)
    }
}

#[derive(Debug)]
pub struct HttpMethodParsingError;

impl std::fmt::Display for HttpMethodParsingError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "Error parsing http method from HTTP request")
    }
}

impl std::str::FromStr for HttpMethod {
    type Err = HttpMethodParsingError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "Get" | "GET" | "get" => Ok(HttpMethod::Get),
            "Post" | "POST" | "post" => Ok(HttpMethod::Post),
            "Head" | "HEAD" | "head" => Ok(HttpMethod::Head),
            "Delete" | "DELETE" | "delete" => Ok(HttpMethod::Delete),
            "Put" | "PUT" | "put" => Ok(HttpMethod::Put),
            "Patch" | "PATCH" | "patch" => Ok(HttpMethod::Patch),
            "Trace" | "TRACE" | "trace" => Ok(HttpMethod::Trace),
            "Options" | "OPTIONS" | "options" => Ok(HttpMethod::Options),
            "Connect" | "CONNECT" | "connect" => Ok(HttpMethod::Connect),
            raw => Ok(HttpMethod::Other(raw.to_string())),
        }
    }
}
