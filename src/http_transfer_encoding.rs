#[derive(Debug, PartialEq, Clone)]
pub enum HttpTransferEncoding {
    Chunked,
    NoEncoding,
}

impl Default for HttpTransferEncoding {
    fn default() -> Self {
        HttpTransferEncoding::NoEncoding
    }
}
#[derive(Debug)]
pub struct TransferEncodingParsingError;

impl std::fmt::Display for TransferEncodingParsingError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "Error parsing transfer-encoding from HTTP Response :(")
    }
}

impl std::str::FromStr for HttpTransferEncoding {
    type Err = TransferEncodingParsingError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if s.to_lowercase().contains("chunked") {
            Ok(HttpTransferEncoding::Chunked)
        } else { 
            Ok(HttpTransferEncoding::NoEncoding)
        }
    }
}
